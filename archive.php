<?php

/**
 * The template for displaying archive pages.
 *
 * @package espresso
 * @since   1.0.0
 */

get_header(); ?>

    <!-- content-area -->
    <section class="content-area">

            <?php if ( have_posts() ) : ?>

                <!-- page-header -->
                <header class="page-header">
                    <?php
                        the_archive_title( '<h1 class="page-title">', '</h1>' );
                        the_archive_description( '<div class="taxonomy-description">', '</div>' );
                    ?>
                </header>
                <!-- /page-header -->

                <?php get_template_part( 'loop' );

            else :

                get_template_part( 'template-parts/content', 'none' );

            endif; ?>

    </section>

<?php
get_sidebar();
get_footer();

<?php

/**
 * Set up Espresso theme
 *
 * @package espresso
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Espresso' ) ) :

    class Espresso {

        /**
         * The Text Domain of the theme.
         *
         * @since    2.1.0
         * @access   protected
         * @var      string    $text_domain    The Text Domain of the theme.
         */
        protected $text_domain;

        public function __construct() {

            $this->text_domain = 'espresso';

        }

        /**
         *
         * @since 1.0.0
         */
        public function init() {

            add_action( 'after_setup_theme',          array( $this, 'setup' ) );
            add_action( 'after_setup_theme',          array( $this, 'cleanup' ) );
            add_action( 'after_setup_theme',          array( $this, 'register_menus' ) );
            add_action( 'after_setup_theme',          array( $this, 'color_palette' ) );
            add_action( 'wp_enqueue_scripts',         array( $this, 'styles' ) );
            add_action( 'admin_enqueue_scripts',      array( $this, 'admin_styles' ), 10 );
            add_action( 'wp_enqueue_scripts',         array( $this, 'scripts' ) );

            add_action( 'wp_head',                    array( $this, 'resource_prefetch' ), 1 );

            add_filter( 'auto_update_theme',           array( $this, 'auto_update_settings' ), 10, 2 );

            add_action( 'enqueue_block_editor_assets',        array( $this, 'admin_scripts' ) );

            add_action( 'map_meta_cap',                array( $this, 'privacy_page_permissions' ), 1, 4 );

        }

        /**
         * Sets up theme defaults and registers support for various WordPress features.
         *
         * @since  1.0.0
         */
        public function setup() {

            /**
             * Load text domain
             */
            load_theme_textdomain( 'espresso', get_template_directory() . '/languages' );

            /**
             * Let WordPress manage the document title.
             */
            add_theme_support( 'title-tag' );

            /**
             * Declare support for selective refreshing of widgets.
             */
            add_theme_support( 'customize-selective-refresh-widgets' );

            /**
             * Enable support for Post Thumbnails on posts and pages.
             *
             * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
             */
            add_theme_support( 'post-thumbnails', array(
                'post',
                'page'
            ) );

            add_theme_support( 'automatic-feed-links' );

            /**
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support( 'html5', array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption'
            ) );

            add_theme_support( 'disable-custom-font-sizes' );
            add_theme_support( 'editor-font-sizes', array() );
            add_theme_support( 'disable-custom-colors' );
            add_theme_support( 'responsive-embeds' );
            add_theme_support( 'align-wide' );

            add_image_size( 'wide', 1920, 800, true );
            add_image_size( 'square', 800, 800, true );
            add_image_size( 'rectangle', 600, 400, true );

        }

        /**
         * Turn off auto update on this theme to avoid
         * name conflicts.
         *
         * @since  2.1.0
         */
        public function auto_update_settings( $update, $item ) {

            // Array of theme slugs to never auto-update
            $themes = array (
                'espresso',
            );

            if ( in_array( $item->slug, $themes ) ) {
                return false; // never update themes in this array
            } else {
                return $update; // Else, use the normal API response to decide whether to update or not
            }

        }

        /**
         * Removes a bunch of default links, scripts and styles that
         * clutter up the <head> section.
         *
         * @since  2.1.0
         */
        public function cleanup() {

            remove_action( 'wp_head',         'rsd_link' );
            remove_action( 'wp_head',         'wlwmanifest_link' );
            remove_action( 'wp_head',         'wp_generator' );
            remove_action( 'wp_head',         'parent_post_rel_link' );
            remove_action( 'wp_head',         'start_post_rel_link' );
            remove_action( 'wp_head',         'index_rel_link' );
            remove_action( 'wp_head',         'adjacent_posts_rel_link' );
            remove_action( 'wp_head',         'adjacent_posts_rel_link_wp_head', 10, 0 );
            remove_action( 'wp_head',         'wp_shortlink_wp_head' );
            remove_action( 'wp_head',         'feed_links',                      2 );
            remove_action( 'wp_head',         'feed_links_extra',                3 );
            remove_action( 'wp_head',         'wp_shortlink_wp_head',            10, 0 );
            remove_action( 'wp_head',         'print_emoji_detection_script',    7 );
            remove_action( 'wp_print_styles', 'print_emoji_styles' );

        }

        /**
         * Registers menu locations for the theme.
         *
         * @since  1.0.0
         */
        public function register_menus() {

            register_nav_menus(
                array(
                    'main-menu'         => __( 'Main Menu', 'espresso' ),
                    'site-map'          => __( 'Site Map', 'espresso' ),
                    'footer-menu'       => __( 'Footer Menu', 'espresso' )
                )
            );

        }

        /**
         * Color Palette for Editor
         *
         * @return Array array of colors with name, slug and hex value
         * @since  3.1.2
         */
        public function color_palette() {

            add_theme_support( 'editor-color-palette', array(

                array(
                    'name'  => __( 'Red', 'espresso' ),
                    'slug'  => 'red',
                    'color' => '#FF0000',
                ),
                array(
                    'name'  => __( 'Green', 'espresso' ),
                    'slug'  => 'green',
                    'color' => '#00FF00',
                ),
                array(
                    'name'  => __( 'Blue', 'espresso' ),
                    'slug'  => 'blue',
                    'color' => '#0000FF',
                )

            ) );

        }

        /**
         * Registers and enqueues front end stylesheets
         *
         * @since  1.0.0
         */
        public function styles() {

            global $theme_version;

            wp_register_style(
                'espresso',
                get_stylesheet_uri(),
                '',
                $theme_version,
                'all'
            );

            wp_enqueue_style( 'espresso' );

        }

        /**
         * Registers and enqueues front end scripts
         *
         * @since  1.0.0
         */
        public function scripts() {

            global $theme_version;

            wp_register_script(
                'main',
                get_theme_file_uri( '/assets/build/js/main.js' ),
                array( 'jquery' ),
                $theme_version,
                true
            );

            wp_enqueue_script( 'main' );

        }

        public function admin_styles() {

            global $theme_version;

            // Register block editor styles for backend.
            wp_register_style(
                'admin', // Handle.
                get_theme_file_uri( 'assets/build/css/admin.css' ), // Block editor CSS.
                array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
                $theme_version,
                'all'
            );

            wp_enqueue_style( 'admin' );

            wp_enqueue_style(
                'fonts',
                'https://use.typekit.net/XXXXXX.css',
                '',
                $theme_version,
                'all'
            );

        }

        public function admin_scripts() {

            global $theme_version;

            wp_register_script(
                'espresso-blocks',
                get_theme_file_uri( 'assets/build/js/blocks.js' ),
                 array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' ),
                $theme_version,
                false
            );

            wp_enqueue_script( 'espresso-blocks' );

        }

        public function resource_prefetch() {
            ?>
                <link rel="preconnect" href="https://use.typekit.net" crossorigin>
                <link rel="preconnect" href="https://fonts.googleapis.com" crossorigin>
            <?php
        }

        public function privacy_page_permissions( $caps, $cap, $user_id, $args ) {
    
            if ( 'manage_privacy_options' === $cap ) {
                $manage_name = is_multisite() ? 'manage_network' : 'manage_options';
                $caps = array_diff($caps, [ $manage_name ]);
            }
        
            return $caps;

        }

    }

endif;
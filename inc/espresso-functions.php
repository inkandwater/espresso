<?php

/**
 * Espresso Functions
 *
 * @package espresso
 * @since   1.0.0
 */

namespace espresso;

if ( ! function_exists( __NAMESPACE__ . '\custom_body_classes' ) ) :

    /**
     *
     * @since  1.0.0
     */
    function custom_body_classes( $classes ) {

        $featured = (
            is_page() ||
            is_singular( 'post' )
        )? true : false;

        if( $featured && has_post_thumbnail() ) {
            $classes[] = 'has-featured-image';
        }

        return $classes;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\custom_post_classes' ) ) :

    /**
     *
     * @since  2.1.0
     */
    function custom_post_classes( $classes ) {

        $classes = array();

        if( is_front_page() ) :
            $classes[] = 'home';
        endif;

        if( has_post_thumbnail() ) :
            $classes[] = 'has-featured-image';
        endif;

        if( 'post' == get_post_type() && !is_single() ) {
            $classes = array( 'post', 'card', 'card--post', 'hentry' );
            if( has_post_thumbnail() ) {
                $classes[] = 'post--has-featured-image';
            }
        } elseif( 'post' == get_post_type() && is_single() ) {
            $classes = array( 'post', 'post--single', 'hentry' );
            if( has_post_thumbnail() ) {
                $classes[] = 'post--has-featured-image';
            }
        }

        array_push( $classes, 'hentry');
        array_push( $classes, get_post_type() );

        return $classes;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\add_copyright_link' ) ) :

    /**
     *
     * @since  2.1.0
     */
    function add_copyright_link( $items, $args ) {

        $copyright_item = sprintf(
            '<li class="navigation__nav-item navigation__nav-item--copyright"><span class="copyright-text text--bold">&copy; %1s %2s</span></li>',
                date( 'Y' ),
                get_bloginfo( 'name' )
        );

        if ( $args->theme_location == 'footer-menu' ) {

            $menu_items = '';
            $menu_items .= $copyright_item . $items;

            return $menu_items;

        } else {

            return $items;

        }

    }

endif;


if ( ! function_exists( __NAMESPACE__ . '\get_header_class' ) ) :

    /**
     * Get classes for site header
     *
     * This function is based on WP's get_body_class.
     *
     * @param  string|string[] $class Space-separated string or array of class names to add to the class list.
     * @return string[] Array of class names.
     */
    function get_header_class( $class = '' ) {

        $classes = array( 'header' );

        // E.g.
        // if( is_page_template( 'templates/tpl-no-header.php' ) ) :
        //     $classes[] = 'header--floating';
        // endif;

        if ( ! empty( $class ) ) {

            if ( ! is_array( $class ) ) {
                $class = preg_split( '#\s+#', $class );
            }

            $classes = array_merge( $classes, $class );

        } else {
            // Ensure that we always coerce class to being an array.
            $class = array();
        }

        $classes = array_map( 'esc_attr', $classes );

        return array_unique( $classes );

    }

endif;
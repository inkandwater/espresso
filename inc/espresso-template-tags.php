<?php

/**
 * Espresso template tags.
 *
 * @package espresso
 * @since   1.0.0
 */

namespace espresso;

if ( ! function_exists( __NAMESPACE__ . '\main_menu' ) ) :

    function main_menu() {

        /**
        * Displays a navigation menu
        * @param array $args Arguments
        */
        $defaults = array(
            'theme_location'  => 'main-menu',
            'container'       => false,
            'menu_class'      => 'navigation navigation--main',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'         => new \Espresso_Nav_Walker( 'navigation' )
        );

        wp_nav_menu( apply_filters( 'espresso_main_menu_args', $defaults ) );

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\small_screen_menu' ) ) :

    function small_screen_menu() {

        /**
        * Displays a navigation menu
        * @param array $args Arguments
        */
        $defaults = array(
            'theme_location'  => 'main-menu',
            'container'       => false,
            'menu_class'      => 'navigation navigation--small-screen',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 1,
            'walker'         => new \In2science_Nav_Walker( 'navigation', true )
        );

        wp_nav_menu( apply_filters( 'espresso_main_menu_args', $defaults ) );

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_map' ) ) : 

    function site_map() {

        $defaults = array(
            'theme_location' => 'site-map',
            'container'      => false,
            'fallback_cb'    => false,
            'echo'           => true,
            'depth'          => 2,
            'menu_class'     => 'navigation navigation--site-map',
            'items_wrap'     => '<ul class="%2$s">%3$s</ul>',
            'walker'         => new \In2science_Nav_Walker( 'navigation' )
        );

        wp_nav_menu( apply_filters( 'espresso_footer_menu_args', $defaults ) );
    
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\footer_terms_menu' ) ) : 

    function footer_terms_menu() {

        $defaults = array(
            'theme_location' => 'footer-terms',
            'container'      => false,
            'fallback_cb'    => false,
            'echo'           => true,
            'depth'          => 2,
            'menu_class'     => 'navigation navigation--terms',
            'items_wrap'     => '<ul class="%2$s">%3$s</ul>',
            'walker'         => new \In2science_Nav_Walker( 'navigation' )
        );

        wp_nav_menu( apply_filters( 'espresso_footer_menu_args', $defaults ) );
    
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\header_class' ) ) :

    function header_class( $class = '' ) {
        echo 'class="' . join( ' ', get_header_class( $class ) ) . '"';
    }

endif;
<?php

/**
 * Espresso template functions.
 *
 * @package espresso
 * @since   1.0.0
 */
namespace espresso;

if ( ! function_exists( __NAMESPACE__ . '\site_icon' ) ) :

    function site_icon() {
        ?>
        
            <link rel="icon" type="image/svg+xml" href="<?php echo get_theme_file_uri( 'assets/build/img/site-icon.svg' ); ?>">

        <?php
    }

endif;

/**
 * Header Functions.
 *
 * @package espresso
 * @since   1.0.0
 * @see     espresso_site_before
 * @see     espresso_header_before
 * @see     espresso_header
 * @see     espresso_content_before
 * @see     espresso_content_top
 */
if ( ! function_exists( __NAMESPACE__ . '\site_branding' ) ) :
    /**
     * Site branding wrapper and display
     */
    function site_branding() {
        ?>

        <!-- site-branding -->
        <div class="site-branding">
            <a class="site-branding__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" aria-label="<?php echo esc_attr( bloginfo( 'name' ) ); ?>">
            </a>
        </div>
        <!-- /site-branding -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\main_navigation' ) ) :
    /**
     * Site branding wrapper and display
     */
    function main_navigation() {

        if( has_nav_menu( 'main-menu' ) ) : ?>

            <!-- main-navigation -->
            <nav id="main-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'espresso' ); ?>">

                <?php main_menu(); ?>

            </nav>
            <!-- /main-navigation -->

        <?php endif;

    }

endif;

/**
 * Footer Functions.
 *
 * @package espresso
 * @since   1.0.0
 * @see     espresso_content_bottom
 * @see     espresso_footer_before
 * @see     espresso_footer
 * @see     espresso_footer_after
 */
if ( ! function_exists( __NAMESPACE__ . '\footer_bar' ) ) :

    function footer_bar() {

        if( is_active_sidebar( 'footer-bar' ) ) :
            ?>

            <!-- footer-bar -->
            <aside class="footer-bar">
                <div class="content-wrapper">
                    <?php dynamic_sidebar( 'footer-bar' ); ?>
                </div>
            </aside>
            <!-- /footer-bar -->

        <?php

        endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_branding_footer' ) ) :

    function site_branding_footer() {
        ?>

        <!-- site-branding -->
        <div class="site-branding site-branding--footer">
            <h1><?php echo get_bloginfo( 'name' ); ?></h1>
            <h2><?php echo get_bloginfo( 'description' ); ?></h2>
        </div>
        <!-- /site-branding -->

        <?php

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_terms' ) ) :

    function site_terms() {
        ?>

        <div class="site-bottom-bar">

        <!-- site-terms -->
        <div class="site-terms">
            <?php

            $defaults = array(
                    'theme_location' => 'footer-menu',
                    'container'      => false,
                    'fallback_cb'    => false,
                    'echo'           => true,
                    'depth'          => 2,
                    'menu_class'     => 'site-copyright',
                    'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            );

            wp_nav_menu( apply_filters( 'espresso_footer_menu_args', $defaults ) ); ?>

        </div>
        <!-- /site-terms -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_social' ) ) :

    function site_social() {
        ?>

        <!-- site-social -->
        <div class="site-social">
            <ul class="site-social__links">
                <?php do_social_media_buttons(); ?>
            </ul>
        </div>
        <!-- /site-social -->

        </div>

        <?php
    }

endif;

/**
 * Post Functions.
 *
 * @package espresso
 * @since   1.0.0
 * @see     espresso_loop_before
 * @see     espresso_loop_after
 * @see     espresso_single_before
 * @see     espresso_single_post_top
 * @see     espresso_single_post
 * @see     espresso_single_post_bottom
 * @see     espresso_single_after
 */
if ( ! function_exists( __NAMESPACE__ . '\display_comments' ) ) :
    /**
     * Espresso display comments
     *
     * @since  1.0.0
     */
    function display_comments() {

        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || '0' != get_comments_number() ) :
            comments_template();
        endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\post_header' ) ) :
    /**
     * Display the post header with a link to the single post
     *
     * @since 1.0.0
     */
    function post_header() {
        ?>

        <!-- entry-header -->
        <header class="post__header entry-header">
            <?php
                if ( is_single() ) {
                    category_list();
                    posted_on();
                    the_title( '<h1 class="post__title entry-title">', '</h1>' );
                } else {

                    if ( 'post' == get_post_type() ) { ?>
                        <aside class="post__meta entry-meta">
                            <?php category_list(); ?>
                            <?php posted_on(); ?>
                        </aside>
                    <?php }

                the_title( sprintf( '<h2 class="post__title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
            }
            ?>
        </header>
        <!-- /entry-header -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\post_thumbnail' ) ) :
    /**
     * Display post thumbnail
     *
     * @var $size thumbnail size. thumbnail|medium|large|full|$custom
     * @uses has_post_thumbnail()
     * @uses the_post_thumbnail
     * @param string $size the post thumbnail size.
     * @since 1.5.0
     */
    function post_thumbnail( $size = 'full' ) {

        $size = apply_filters( 'espresso_post_thumbnail_size', $size );

        if ( has_post_thumbnail() ) { ?>

            <!-- entry-thumbnail -->
            <div class="post__thumbnail entry-thumbnail">
                <?php the_post_thumbnail( $size ); ?>
            </div>
            <!-- /entry-thumnbail -->

        <?php }
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\post_content' ) ) :
    /**
     * Display the post content with a link to the single post
     *
     * @since 1.0.0
     */
    function post_content() {
        $class = ( is_single() )? 'entry-content' : 'entry-summary';
        ?>
            <!-- entry-content -->
            <div class="post__content <?php echo esc_attr( $class ); ?> content-block">
                <?php

                /**
                 * Functions hooked in to espresso_post_content_before action.
                 *
                 * @hooked espresso_post_thumbnail - 10
                 */
                do_action( 'espresso_post_content_before' );

                if( is_single() ) :
                    the_content(
                        sprintf(
                            __( 'Continue reading %s', 'espresso' ),
                            '<span class="screen-reader-text">' . get_the_title() . '</span>'
                        )
                    );
                else :
                    the_excerpt();
                endif;

                do_action( 'espresso_post_content_after' );

                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'espresso' ),
                    'after'  => '</div>',
                ) );
                ?>
            </div>
            <!-- /entry-content -->
        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\paging_nav' ) ) :
    /**
     * Display navigation to next/previous set of posts when applicable.
     */
    function paging_nav() {
        global $wp_query;

        $args = array(
            'type'      => 'list',
            'next_text' => _x( 'Next', 'Next post', 'espresso' ),
            'prev_text' => _x( 'Previous', 'Previous post', 'espresso' ),
            );

        the_posts_pagination( $args );
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\post_nav' ) ) :
    /**
     * Display navigation to next/previous post when applicable.
     */
    function post_nav() {

        $args = array(
            'prev_text' => __( '<span class="nav-arrow">Older</span> <span class="nav-previous-title">%title</span>', 'espresso' ),
            'next_text' => __( '<span class="nav-arrow">Newer</span> <span class="nav-next-title">%title</span>', 'espresso' ),
            );
        ?>

        <!-- post-navigation -->
        <?php the_post_navigation( $args ); ?>
        <!-- /post-navigation -->

        <?php

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\category_list' ) ) :

    function category_list() {

        /* translators: used between list items, there is a space after the comma */
        $categories_list = get_the_category_list( esc_html__( ', ', 'espresso' ) );

        if ( $categories_list ) :
            printf( '<div class="categories-links"><span class="categories-label">%s</span> %s</div>',
                esc_attr( __( 'Categories', 'espresso' ) ),
                wp_kses_post( $categories_list )
            );
        endif; // End if $tags_list.

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\tag_list' ) ) :

    function tag_list() {

        /* translators: used between list items, there is a space after the comma */
        $tags_list = get_the_tag_list( '', __( ', ', 'espresso' ) );

        if ( $tags_list ) :
            printf( '<div class="tags-links"><span class="tags-label">%s</span> %s</div>',
                esc_attr( __( 'Tagged', 'espresso' ) ),
                wp_kses_post( $tags_list )
            );
        endif; // End if $tags_list.

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\posted_on' ) ) :
    /**
     * Prints HTML with meta information for the current post-date/time and author.
     */
    function posted_on() {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time> <time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf( $time_string,
            esc_attr( get_the_date( 'c' ) ),
            esc_html( get_the_date() ),
            esc_attr( get_the_modified_date( 'c' ) ),
            esc_html( get_the_modified_date() )
        );

        $posted_on = sprintf( '<a href="%1s" rel="bookmark">%2s</a>', esc_url( get_permalink() ), $time_string );

        echo wp_kses( apply_filters( 'espresso_single_post_posted_on_html', '<div class="posted-on">' . $posted_on . '</div>', $posted_on ), array(
            'div' => array(
                'class'  => array(),
            ),
            'a'    => array(
                'href'  => array(),
                'title' => array(),
                'rel'   => array(),
            ),
            'time' => array(
                'datetime' => array(),
                'class'    => array(),
            ),
        ) );
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\posted_by' ) ) :

    function posted_by() {
        ?>
            <div class="author-meta">
                <?php printf( '<div class="avatar">%s</div>', get_avatar( get_the_author_meta( 'ID' ), 48 ) ); ?>
                <?php printf( '<b class="label">%s</b>', get_the_author_posts_link() ); ?>
            </div>

        <?php
    }

endif;

/**
 * Page Functions.
 *
 * @package espresso
 * @since   1.0.0
 * @see     espresso_page_before
 * @see     espresso_page_after
 */
if ( ! function_exists( __NAMESPACE__ . '\page_header' ) ) :

    function page_header( $size = 'full' ) {

        $size = apply_filters( 'espresso_page_header_thumbnail_size', $size );
        ?>

        <!-- entry-header -->
        <header class="page__header entry-header">

            <?php if( has_post_thumbnail() ) :
                the_post_thumbnail( $size );
            endif; ?>

            <?php the_title( '<h1 class="page__title entry-title">', '</h1>' ); ?>

        </header>
        <!-- /entry-header -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\page_content' ) ) :

    function page_content() {
        ?>

        <!-- entry-content -->
        <div class="page__content entry-content content-block">
            <?php the_content(); ?>
            <?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'espresso' ),
                    'after'  => '</div>',
                ) );
            ?>
        </div>
        <!-- /entry-content -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\get_sidebar' ) ) :
    /**
     * Display espresso sidebar
     *
     * @uses get_sidebar()
     * @since 1.0.0
     */
    function get_sidebar() {

        get_sidebar();

    }

endif;

/**
 * Homepage Functions.
 *
 * @package espresso
 * @since   1.0.0
 *
 */
if ( ! function_exists( __NAMESPACE__ . '\homepage_content' ) ) :

    function homepage_content() {

        if( have_posts() ) :

            echo '<section class="homepage-content homepage-section" aria-label="Homepage Content">';

            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'page' );

            endwhile;

            wp_reset_postdata();

            echo '</section>';

        endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\homepage_posts' ) ) :

    function homepage_posts() {

        $args = array(
            'post_type'         => 'post',
            'posts_per_page'    => 5
        );

        $homepage_posts = new WP_Query( $args );

        if( $homepage_posts->have_posts() ) :

            echo '<section class="homepage-posts homepage-section" aria-label="Homepage Posts">';

            $title = sprintf( '<h2 class="section-title">%s</h2>', __( 'Posts', 'espresso' ) );
            $title = apply_filters( 'espresso_homepage_posts_section_title', $title );

            echo wp_kses_post( $title );

            while ( $homepage_posts->have_posts() ) : $homepage_posts->the_post();

                get_template_part( 'template-parts/content', get_post_format() );

            endwhile;

            wp_reset_postdata();

            echo '</section>';

        endif;

    }

endif;

/**
 * 404 Functions.
 *
 * @package espresso
 * @since   1.0.0
 * @see     espresso_404_page_content
 */
if ( ! function_exists( __NAMESPACE__ . '\error_404_header' ) ) :

    function error_404_header() {
        ?>
            <!-- page-header -->
            <header class="page-header">
                <h1 class="page-title"><?php esc_html_e( '404. Page Not Found', 'espresso' ); ?></h1>
            </header>
            <!-- /page-header -->
        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\error_404_text' ) ) :

    function error_404_text() {
        ?>
            <!-- page-content -->
            <div class="page-content">
                <p><?php esc_html_e( 'There\'s no page at this location (either it\'s moved, or you typed in the URL incorrectly). To get back on track, try one of the links in the header or do a search for what you\'re after:', 'espresso' ); ?></p>
            </div>
            <!-- /page-content -->
        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_search' ) ) :

    function site_search() {
        ?>
            <!-- site-search -->
            <section aria-label="Search">
                <?php get_search_form(); ?>
            </section>
            <!-- /site-search -->
        <?php
    }

endif;
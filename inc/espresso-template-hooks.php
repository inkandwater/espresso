<?php

/**
 * Espresso hooks
 *
 * @package espresso
 * @since   1.0.0
 */

/**
 * General
 *
 */
add_action( 'espresso_sidebar',                   'espresso\\get_sidebar',            10 );

add_filter( 'wp_nav_menu_items',                   'espresso\\add_copyright_link',     10, 2 );
add_filter( 'body_class',					       'espresso\\custom_body_classes',    10, 1 );
add_filter( 'post_class',					       'espresso\\custom_post_classes',    10, 1 );

/**
 * Header
 *
 * @see espresso_header_before
 * @see espresso_header
 * @see espresso_content_top
 */
add_action( 'espresso_header',                    'espresso\\site_branding',          10 );
add_action( 'espresso_header',                    'espresso\\main_navigation',        20 );

/**
 * Footer
 *
 * @see espresso_footer_before
 * @see espresso_footer
 * @see espresso_footer_after
 */
add_action( 'espresso_footer_before',             'espresso\\footer_bar',             10 );
add_action( 'espresso_footer',                    'espresso\\site_terms',             20 );

/**
 * Pages
 *
 * @see espresso_page
 */
add_action( 'espresso_page',                      'espresso\\page_header',            10 );
add_action( 'espresso_page',                      'espresso\\page_content',           20 );

/**
 * Posts
 *
 * @see espresso_loop_before
 * @see espresso_loop_post
 * @see espresso_loop_after
 * @see espresso_single_post
 * @see espresso_single_after
 * @see espresso_single_post_bottom
 */
add_action( 'espresso_loop_post',                 'espresso\\post_header',            10 );
add_action( 'espresso_loop_post',                 'espresso\\post_thumbnail',         20 );
add_action( 'espresso_loop_post',                 'espresso\\post_content',           30 );
add_action( 'espresso_loop_after',                'espresso\\paging_nav',             10 );
add_action( 'espresso_single_post',               'espresso\\post_header',            10 );
add_action( 'espresso_single_post',               'espresso\\post_thumbnail',         20 );
add_action( 'espresso_single_post',               'espresso\\post_content',           30 );
add_action( 'espresso_single_after',              'espresso\\post_nav',               10 );
add_action( 'espresso_single_post_bottom',        'espresso\\display_comments',       20 );

/**
 * 404
 *
 * @see espresso_404_page_content
 */
add_action( 'espresso_404_page_content',          'espresso\\404_header',             10 );
add_action( 'espresso_404_page_content',          'espresso\\404_text',               20 );
add_action( 'espresso_404_page_content',          'espresso\\site_search',            30 );
<?php

/**
 * Custom Nav Walker
 *
 * Cleans up WP menu classes to use BEM.
 * 
 * @package espresso
 * @since   2.1.0
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Espresso_Nav_Walker' ) ) :

    class Espresso_Nav_Walker extends Walker_Nav_Menu {

        public function __construct( $slug = 'menu', $animation = false ) {
            $this->slug = $slug;
            $this->animation = $animation;
        }

        public function start_lvl( &$output, $depth = 0, $args = array() ) {
            $output .= sprintf( '<ul class="%s__sub-menu">', esc_attr( $this->slug ) );
        }

        public function end_lvl( &$output, $depth = 0, $args = array() ) {
            $output .= '</ul>';
        }

        public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
            $classes = array();

            if( !empty( $item->classes ) ) {
                $classes = (array) $item->classes;
            }

            $custom_classes = array( sprintf( '%s__nav-item', esc_attr( $this->slug ) ) );

            // If any custom classes are required, other than the ones 
            // whitelisted below, then uncomment these next lines.

            // $user_defined_classes = get_post_meta( $item->object_id, '_menu_item_classes', true );
            
            // if( is_array( $user_defined_classes ) ) :
            //     $custom_classes[] = implode( $user_defined_classes, ' ' );
            // endif;

            // Swap the classes we need from $classes with our own custom class names
            if( in_array('current-menu-item', $classes) ) {
                $custom_classes[] = sprintf( '%s__nav-item--current', esc_attr( $this->slug ) );
            }

            if( in_array('current-menu-parent', $classes) ) {
                $custom_classes[] = sprintf( '%s__nav-item--current-parent', esc_attr( $this->slug ) );
            }

            if( in_array('current-menu-ancestor', $classes) ) {
                $custom_classes[] = sprintf( '%s__nav-item--current-ancestor', esc_attr( $this->slug ) );
            }

            if( in_array( 'menu-item-has-children', $classes) ) {
                $custom_classes[] = sprintf( '%s__nav-item--has-children', esc_attr( $this->slug ) );
            }

            $url = '';

            if( !empty( $item->url ) ) {
                $url = $item->url;
            }

            $custom_classes = implode( ' ', $custom_classes );
            
            if ( $this->animation ) {
                $output .= sprintf( '<li class="%s" style="--animation-order: %s;"><a href="%s">%s</a>', 
                    esc_attr( $custom_classes ), 
                    esc_attr( $item->menu_order ),
                    esc_url( $url ), 
                    esc_html( $item->title ) 
                );
            } else {
                $output .= sprintf( '<li class="%s"><a href="%s">%s</a>', esc_attr( $custom_classes ), esc_url( $url ), esc_html( $item->title ) );
            }

        }

        public function end_el( &$output, $item, $depth = 0, $args = array() ) {
            $output .= '</li>';
        }
    }

endif;
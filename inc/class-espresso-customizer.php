<?php

/**
 * Espresso Customizer Class
 *
 * @package espresso
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Espresso_Customizer' ) ) :

    /**
     * The Espresso Customizer class
     */
    class Espresso_Customizer {


        public function __construct() {


        }

        public function customizer_init() {

            add_action( 'customize_register', array( $this, 'customize_register' ), 10 );

            // Enqueue live preview javascript in Theme Customizer admin screen
            add_action( 'customize_preview_init' , array( $this, 'live_preview' ) );

        }

        public function customize_register( $wp_customize ) {


        }

        public function live_preview( ) {

        }

    }

endif;
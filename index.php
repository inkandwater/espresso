<?php

/**
 * The main template file.
 *
 * @package espresso
 * @since   1.0.0
 */

get_header(); ?>

    <!-- content-area -->
    <section class="content-area">

        <?php if ( have_posts() ) :

            get_template_part( 'loop' );

        else :

            get_template_part( 'template-parts/content', 'none' );

        endif; ?>

    </section>
    <!-- /content-area -->

<?php
get_footer();
=== Espresso ===

Contributors: tomnapier, paulrmyers
Requires at least: 4.4
Tested up to: 4.9.6
Stable tag: 2.1.2
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

== Description ==

Espresso is a lean parent theme using bourbon, neat and modular scale.

== Changelog ==

* 3.1.2 *

- Minor changes to npm scripts and Gulpfile to include some gutenberg-focussed features.

* 3.1.0 *

- Revise build process for assets (now uses wp-scripts)
- Better support for Gutenberg features.

* 3.0.0 *
- Refactor colour palette usage
- Move Normalize to NPM module

* 2.2.0 *
- Removed redundant template functions.
- Removed shortcodes and off-canv classes.
- Removed JavaScript.
- Tidied the functions.
- Refactor of all the page templates

* 2.1.2 *
- Correct endif syntax error.

* 2.1.1 *
- Correct 404 function name syntax error.

* 2.1.0 *
- Added Shortcodes class.
- Updated docblocks.

* 2.0.0 *
- Added namespacing and custom nav walker.

* 1.2.1 *
- Updated comments, removed more styling, added a screenshot!

* 1.2.0 *
- Minified scripts and removed some OTT styling.

* 1.1.0 *
- Updated using 'Cortado' starter theme.

* 1.0.0 *

== Find and Replace Rules ==

espresso
espresso_
Espresso
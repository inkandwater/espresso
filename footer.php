<?php

/**
 * The template for displaying the footer.
 *
 * @package espresso
 * @since   1.0.0
 */

/**
 * Functions hooked into espresso_content_bottom
 *
 */
do_action( 'espresso_content_bottom' ); ?>

</div><!-- /site-content -->

    <?php
    /**
     * Functions hooked into espresso_footer_before
     *
     * @see 10 espresso_footer_bar
     */
    do_action( 'espresso_footer_before' ); ?>

    <!-- footer -->
    <footer id="footer" class="footer">
        <?php
        /**
         * Functions hooked into espresso_footer
         *
         * @see 10 espresso_footer_widgets
         * @see 20 espresso_site_terms
         */
        do_action( 'espresso_footer' ); ?>
    </footer>
    <!-- /footer -->

    <?php
    /**
     * Functions hooked into espresso_footer_before
     *
     * @see 10 espresso_off_canvas_close
     */
    do_action( 'espresso_footer_after' ); ?>

</div>

<!-- scripts -->
<?php wp_footer(); ?>
<!-- /scripts -->

</body>
</html>
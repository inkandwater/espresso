<?php

/**
 * Serve up a new Espresso!
 *
 * @package espresso
 * @since   1.0.0
 */

/**
 * Assign the theme version to a var
 */
$theme              = wp_get_theme( 'espresso' );
$theme_version      = $theme['Version'];

require 'inc/espresso-template-tags.php';
require 'inc/espresso-template-hooks.php';
require 'inc/espresso-template-functions.php';
require 'inc/espresso-functions.php';

require 'inc/class-espresso-nav-walker.php';
require 'inc/class-espresso-customizer.php';
require 'inc/class-espresso.php';

$startup = new Espresso();
$startup->init();

$customizer = new Espresso_Customizer();
$customizer->customizer_init();
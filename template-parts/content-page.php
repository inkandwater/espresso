<?php

/**
 * Template used for displaying page content in page.php
 *
 * @package espresso
 * @since  1.0.0
 */

?>

<!-- page-<?php the_ID(); ?> -->
<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked into espresso_page_top
     *
     */
    do_action( 'espresso_page_top' );

    /**
     * Functions hooked into espresso_page
     * 
     * @see 10 espresso_page_header
     * @see 20 espresso_page_content
     */
    do_action( 'espresso_page' );

    /**
     * Functions hooked in to espresso_page_bottom
     *
     */
    do_action( 'espresso_page_bottom' );
    ?>

</div>
<!-- /page-<?php the_ID(); ?> -->

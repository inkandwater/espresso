<?php

/**
 * Template used to display post content.
 *
 * @package espresso
 * @since  1.0.0
 */

?>

<!-- post-<?php the_ID(); ?> -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked in to espresso_loop_post
     * 
     * @see 10 espresso_post_header
     * @see 20 espresso_post_thumbnail
     * @see 30 espresso_post_content
     * @see 40 espresso_post_meta
     */
    do_action( 'espresso_loop_post' );
    ?>

</article>
<!-- /post-<?php the_ID(); ?> -->

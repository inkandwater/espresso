<?php

/**
 * Template used for displaying post content on single pages.
 *
 * @package espresso
 * @since  1.0.0
 */

?>

<!-- post-<?php the_ID(); ?> -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked into espresso_single_post_top
     *
     */
    do_action( 'espresso_single_post_top' );

    /**
     * Functions hooked into espresso_single_post
     * 
     * @see 10 espresso_post_header
     * @see 20 espresso_post_thumbnail 
     * @see 30 espresso_post_content
     * @see 40 espresso_post_meta
     */
    do_action( 'espresso_single_post' );

    /**
     * Functions hooked in to espresso_single_post_bottom
     *
     * @see 10 espresso_display_comments 
     */
    do_action( 'espresso_single_post_bottom' );
    ?>

</div>
<!-- /post-<?php the_ID(); ?> -->

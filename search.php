<?php

/**
 * The template for displaying search results pages.
 *
 * @package espresso
 * @since   1.0.0
 */
get_header(); ?>

    <!-- content-area -->
    <section class="content-area">

        <!-- search-results -->
        <div class="search-results">

            <?php
            /**
             * Functions hooked into espresso_search_results_content
             *
             */
            do_action( 'espresso_search_results_content' ); ?>

        </div><!-- /search-results -->

    </section>

<?php get_footer();
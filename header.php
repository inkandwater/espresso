<?php

/**
 * The header for our theme.
 *
 * @package espresso
 * @since   1.0.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<!-- head -->
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php wp_head(); ?>

</head>
<!-- /head -->

<!-- body -->
<body <?php body_class(); ?>>

    <?php
    /**
     * Functions hooked into espresso_site_before
     *
     * @see 10 espresso_off_canvas
     */
    do_action( 'espresso_site_before' ); ?>

    <div id="page" class="hfeed site">
        <?php
        /**
         * Functions hooked into espresso_header_before
         *
         */
        do_action( 'espresso_header_before' ); ?>

        <!-- header -->
        <header id="header" class="header">
            <?php
            /**
             * Functions hooked into espresso_header
             *
             * @see 10 espresso_menu_toggle
             * @see 20 espresso_site_branding
             * @see 30 espresso_main_navigation
             */
            do_action( 'espresso_header' ); ?>
        </header>
        <!-- /header -->

        <?php
        /**
         * Functions hooked into espresso_content_before
         *
         */
        do_action( 'espresso_content_before' ); ?>

        <!-- site-content -->
        <div class="site-content">

            <?php
            /**
             * Functions hooked into espresso_content_top
             *
             */
            do_action( 'espresso_content_top' );?>
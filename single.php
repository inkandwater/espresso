<?php

/**
 * The template for displaying all single posts.
 *
 * @package espresso
 * @since   1.0.0
 */

get_header(); ?>

    <!-- content-area -->
    <section class="content-area">

        <?php while ( have_posts() ) : the_post();

            /**
             * Functions hooked into espresso_single_before
             *
             */
            do_action( 'espresso_single_before' );

            get_template_part( 'template-parts/content', 'single' );

            /**
             * Functions hooked into espresso_single_after
             *
             * @see 10 espresso_post_nav
             */
            do_action( 'espresso_single_after' );

        endwhile; ?>

    </section>

<?php
get_footer();
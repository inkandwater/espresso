<?php

/**
 * The loop template file.
 *
 * Included on pages like index.php, archive.php and search.php to display a loop of posts
 * Learn more: http://codex.wordpress.org/The_Loop
 *
 * @package espresso
 * @since   1.0.0
 */

/**
 * Functions hooked into espresso_loop_before
 * 
 */
do_action( 'espresso_loop_before' );

while ( have_posts() ) : the_post();

    /**
     * Include the Post-Format-specific template for the content.
     * If you want to override this in a child theme, then include a file
     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
     */
    get_template_part( 'template-parts/content', get_post_format() );

endwhile;

/**
 * Functions hooked into espresso_loop_after
 *
 * @see 10 espresso_paging_nav
 */
do_action( 'espresso_loop_after' );
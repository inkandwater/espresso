import toggle from './nav-toggle.js';
import './tabbing-navigation.js';

( function( $ ) {

  $( document ).ready( function() {

    $( 'body' ).removeClass( 'no-js' );

    let navToggle = new toggle();
    navToggle.init();

  });

})( jQuery );
/**
 * toggle.js
 *
 * Handles toggling off-screen navigation
 *
 */
export default class navigation {

  constructor() {

    this.settings = {
      toggle      : document.getElementById( 'nav-toggle' ),
      toggleClose : document.getElementById( 'nav-toggle-close' ),
      body        : document.getElementsByTagName( 'body' )[0],
      activeClass : 'js-nav-open'
    }

  }

  init() {

    if ( null !== this.settings.toggle ) {
      this.bindUIActions();
    }

  }

  bindUIActions() {

    let s = this.settings;

    let activeClass  = 'js-dropdown-active';
    let menuItems    = document.getElementsByClassName("main-menu__nav-item--has-children");

    window.addEventListener( 'resize', () => {
      if ( window.innerWidth > 899 ) {
        this.activeClass( 'remove' );
        for (var i = 0; i < menuItems.length; i++) {
          menuItems[i].classList.remove(activeClass);
        }
      }
    } )

    s.toggle.addEventListener( 'click', () => {
      this.activeClass();
    });

  }

  activeClass( status ) {

    let s = this.settings;

    let activeClass  = 'js-dropdown-active';
    let menuItems    = document.getElementsByClassName("main-menu__nav-item--has-children");

    if ( s.body.classList.contains( s.activeClass ) ) {
      s.body.classList.remove( s.activeClass );
      for (var i = 0; i < menuItems.length; i++) {
        menuItems[i].classList.remove(activeClass);
      }
    } else if ( 'remove' === status ) {

      s.body.classList.remove( s.activeClass );

    } else {
      s.body.classList.add( s.activeClass );
    }

  }

}